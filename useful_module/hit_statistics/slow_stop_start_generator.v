//==================================================================================================
//  Filename      : slow_stop_start_generator.v
//  Created On    : 2018-10-18 17:34:05
//  Last Modified : 2018-10-18 17:52:12
//  Revision      : 
//  Author        : Yu Liang
//  Company       : University of Michigan
//  Email         : liangum@umich.edu
//
//  Description   : 
//
//
//==================================================================================================
module slow_stop_start_generator(
	input clk40M,
	input [19:0] windows,
	output start,
	output stop
	)

endmodule 